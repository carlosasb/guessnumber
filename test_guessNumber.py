###======================================================================#
#                                                                        #
# Copyright (C) 2015 Carlos Augusto de Souza Braga                       #
# <CASBraga@Gmail.com>                                                   #
#                                                                        #
#  This program is free software: you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation, either version 3 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#========================================================================r

import guessNumber

# creates GuessNumber object
my_game = guessNumber.GuessNumber(3)

# gets user guess
guess = raw_input("")
my_game.input_guess(guess)

guess = raw_input("")
my_game.input_guess(guess)

guess = raw_input("")
my_game.input_guess(guess)

guess = raw_input("")
my_game.input_guess(guess)

guess = raw_input("")
my_game.input_guess(guess)

guess = raw_input("")
my_game.input_guess(guess)

guess = raw_input("")
my_game.input_guess(guess)

guess = raw_input("")
my_game.input_guess(guess)

# starts new game with diferent ranges of numbers
my_game.range100()

my_game.range1000()
