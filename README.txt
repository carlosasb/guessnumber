###======================================================================#
#                                                                        #
# Copyright (C) 2015 Carlos Augusto de Souza Braga                       #
# <CASBraga@Gmail.com>                                                   #
#                                                                        #
#  This program is free software: you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation, either version 3 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#========================================================================#

This is a README file for the Python class GuessNumber which plays a game of 
Guess the number. Written in Python 2.7.10.

This class was made with the intent of being used in a windowing system 
that calls the event handler functions. While these should work with 
most libraries, such as GTK+, as the event handler functions are designed
to be system agnostic, the messages are still printed to the 
console. This is because an event handler for messages has not been
written yet. Indeed, the game is rather verbose in describing the 
state of the game.

To test the class run test_guessNumber.py in the terminal. There is a version 
which uses the simplegui package (see http://www.codeskulptor.org/# for 
more information) which can be found at 
http://www.codeskulptor.org/#user40_v4fkf14kKqTGLXD_4.py. To run this 
version just press the run button and enjoy.

TODO:

Make it less verbose and keep some messages always written in the 
screen (player and computer score, for instance).

Add GTK+ compatibility. This will take a while, since I am a veritable 
noob at GTK+.

Make it print to the simplegui screen, rather than to the console, in 
codeskulptor. This should be simple enough.
