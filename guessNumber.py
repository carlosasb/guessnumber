###======================================================================#
#                                                                        #
# Copyright (C) 2015 Carlos Augusto de Souza Braga                       #
# <CASBraga@Gmail.com>                                                   #
#                                                                        #
#  This program is free software: you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation, either version 3 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#========================================================================#

import math
import random

class GuessNumber:
    """
    Class GuessNumber is a game of "Guess the number".

    Contains:
    
    Global variables:
        secret_num => integer containing the secret number 
        max_gess   => integer containing the maximum number of guesses allowed 
        num_guess  => integer containing the number of guesses already taken

    Methods:
        private methods (aka helper functions):
            __init__(nl,nh)
            new_game(nlow,nhigh)
        public methods (aka event handlers):
            range100()
            range1000()
            input_guess(guess)
    """

    secret_num = None
    max_guess  = None
    num_guess  = None

    def __init__(self, nl, nh):
        """
        Constructor for the class
        
        nl  => lower boundary for number range
        nh => higher boundary for number range
        """
        
        # calls new_game()
        self.new_game(nl, nh)

    # helper function to start and restart the game
    def new_game(self, nlow, nhigh):
        """
        Iitializes a new game of 'Guess the number'
        
        nlow  => lower boundary for number range
        nhigh => higher boundary for number range
        """
        
        # global variables initialization
        # num_guess is set to 0 any time a new game is initiated
        self.secret_num = random.randrange(nlow, nhigh)
        self.max_guess  = int(math.log(nhigh - nlow + 1, 2)) + 1
        self.num_guess  = 0
        
        # prints new game message
        print "New game! Secret number is between", nlow, "and", nhigh,"."
        print "You have", self.max_guess, "tries left."
        
        # prints empty line
        print ""

    # define event handlers for control panel
    def range100(self):
        """
        Sets range of possible values as [0,100) 
        and starts a new game
        """
        
        # calls new_game with nlow = 0 and nhigh = 100
        self.new_game(0, 100)
    
    def range1000(self):
        """
        Sets range of possible values as [0,1000)
        and starts a new game
        """
        
        # calls new_game with nlow = 0 and nhigh = 1000
        self.new_game(0, 1000)

    # handler containing game logic
    def input_guess(self, guess):
        """
        Evaluates the guess nased on the secret number
        
        guess => string containing the player's guess
        """
        
        # local variables
        num = 0
 
        # try block checks if input is integer
        try:
            num = int(guess)
            # print player's guess
            print "Your guess was", guess
            
            # counts the guess
            self.num_guess += 1
            print "You have", self.max_guess - self.num_guess, "tries left."
            
            # compares the difference between the max_guess and num_guess
            # also compares secret_num and num = int(guess)
            if (self.max_guess - self.num_guess >= 0 and num == self.secret_num):
                print "Correct!"
                print ""
                new_game(0,100)
            elif (self.max_guess - self.num_guess > 0 and num < self.secret_num):
                print "Higher!"
                print ""
            elif (self.max_guess - self.num_guess > 0 and num > self.secret_num):
                print "Lower!"
                print ""
            else:
                print "You have used all your tries! The secret number is", self.secret_num, "." 
                print ""
                self.new_game(0,100)
        except ValueError:
            # prints messages acknowlegding that guess is not an integer
            # and asks the user for new input
            print guess, "is not a valid integer!"
            print "Please, try again!"
            print ""
